from bob.errors import BobError
from bob import git

from subprocess import Popen, PIPE, STDOUT
from shlex import quote, split
from urllib.parse import urlparse
import pprint
import os
import shutil

def pull(name):
    """"pull docker image"""
    job = Popen(["docker", "pull", name], stdout=PIPE, stderr=STDOUT)
    job.wait()
    if job.returncode != 0:
        raise BobError("Something went wrong while pulling docker image")

def run(image, command):
    """run docker image with command"""
    exe = "{}".format(command)
    command = "docker run -it --rm {0} {1}".format(quote(image), exe)
    job = Popen([command], shell=True, stdout=PIPE, stderr=STDOUT)
    job.wait()
    if job.returncode != 0:
        raise BobError("something went wrong while running {0} on image {1}".format(command, image))
    output, _ = job.communicate()
    return output.decode('utf-8')

def build(repository_url, image_name):
    r_name = git.get_repository_name(repository_url)
    r_path = '/tmp/'+r_name
    git.clone(repository_url, r_path)
    job = Popen(["docker build -t {0} .".format(image_name)], shell=True, cwd=r_path, stdout=PIPE, stderr=STDOUT)
    job.wait()
    if job.returncode != 0:
        raise BobError("Failure on building docker image.")
        shutil.rmtree(r_path)
    output, _ = job.communicate()
    return output.decode('utf-8')

def create(image_name):
    job = Popen(["docker create {0}".format(image_name)], shell=True, stdout=PIPE, stderr=STDOUT)
    job.wait()
    if job.returncode != 0:
        raise BobError("Failure on building docker image.")
    output, _ = job.communicate()
    return output.decode('utf-8')

def run_build_image(image_name):
    job = Popen(["docker run {0}".format(image_name)], shell=True, stdout=PIPE, stderr=STDOUT)
    job.wait()
    if job.returncode != 0:
        raise BobError("Failure on running docker image.")
    output, _ = job.communicate()
    return output.decode('utf-8')