import multiprocessing as mp
from subprocess import Popen, PIPE, STDOUT
import pprint
import shutil

from bob import git
from bob.errors import BobError
from bob.config import parse
from bob import docker

# 16:23 <@shiyu|work> misattributed quotes: "Nobody has the intention of building a wall" - bob the builder
# 16:30 <@Hertog> berkeley: btw shouldn't it be BobTheBuilder ? :P
class BobTheBuilder:
    """Bob The Builder will build great code for you! But no walls."""
    def __init__(self):
        self._bob = {}
    
    def init_bob(self, mqueue):
        """you need to initialize bob before he can build your code."""
        queue = mp.Queue()
        pool = mp.Pool(None, head_builder,(queue, mqueue))

        self._bob = {
            'queue': queue,
            'pool': pool
        }

    @property
    def queue(self):
        return self._bob['queue']
        
    @property
    def pool(self):
        return self._bob['pool']

buildbob = BobTheBuilder()

def head_builder(queue, mqueue):
    while True:
        data = queue.get(True)
        builders(data, mqueue)

def builders(data, mqueue):
    result = {'status': None, 'log': None}
    try:
        if 'docker' in data:
            if data['docker']['pull']:
                docker.pull(data['docker']['image']) # blocking
            if data['docker']['build']:
                docker.build(data['docker']['git_repository'], data['docker']['image']) # blocking
                docker.create(data['docker']['image']) # blocking
                output = docker.run_build_image(data['docker']['image'])
                result['status'] = 'succes'
                result['log'] = output
            else:
                job = docker.run(data['docker']['image'], data['docker']['command']) # blocking
                result['log'] = job
                result['status'] = 'succes'
        elif 'git' in data:
            git.clone(data['git']['repository_url'], data['git']['repository_path'])
            git.checkout(data['git']['repository_branch'], data['git']['repository_path'])
            config = parse(data['git']['repository_path']+'/.bob.yaml')
            if 'run' in config:
                job = Popen(config['run'], stdout=PIPE, stderr=STDOUT, shell=True, cwd=data['git']['repository_path'])
                output, _ = job.communicate()
                result['log'] = output.decode('utf-8')
                result['status'] = 'succes'
                if job.returncode != 0:
                    result['status'] = 'Failed'
    except BobError as e:
        result['log'] = repr(e)
        result['status'] = 'Failed'
        pprint.pprint(repr(e))
    if 'clean_data' in data:
        if data['clean_data'] is True:
            shutil.rmtree(data['git']['repository_path'])
    mqueue.put(result)
