import multiprocessing as mp

class WendyTheAssistant:
    """Wendy will help you with communication"""
    def __init__(self):
        self._wendy = {}
    def init_wendy(self):
        queue = mp.Queue()

        self._wendy = {
            'queue': queue,
        }
    @property
    def mqueue(self):
        return self._wendy['queue']
    
wendy = WendyTheAssistant()