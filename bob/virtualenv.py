from subprocess import Popen, STDOUT, PIPE
from bob.errors import BobError
from bob import git

import shutil
def create_virtualenv(name):
    job = Popen(['virtualenv', '/tmp/{}'.format(name)], stderr=STDOUT, stdout=PIPE)
    job.wait()
    if job.returncode != 0:
        raise BobError("Cannot create virtualenv.")
    output, _ = job.communicate()
    return output.decode('utf-8')

def remove_virtualenv(name):
    try:
        shutil.rmtree('/tmp/{}'.format(name))
    except OSError:
        raise BobError("Cannot remove virtualenv")