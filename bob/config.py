# BSD 3-Clause License

# Copyright (c) 2016, Rob Haverkamp
# All rights reserved.

import yaml
from bob.errors import BobError

def parse(file):
    try:
        with open(file) as stream:
            config = yaml.load(stream)
    except OSError:
        raise BobError("Can not open yaml file")
    return config
