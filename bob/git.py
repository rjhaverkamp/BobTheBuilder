# BSD 3-Clause License

# Copyright (c) 2016, Rob Haverkamp
# All rights reserved.

from subprocess import Popen, PIPE, STDOUT
from bob.errors import BobError

from urllib.parse import urlparse
import pprint
import os

def clone(repository_url, path):
    """"git clone repository_url into path."""
    job = Popen(["git", "clone", repository_url, path], stdout=PIPE, stderr=STDOUT)
    job.wait() # this will lock until a returncode is set.
    if job.returncode != 0:
        raise BobError("Something went wrong while clonening {0} to {1}".format(repository_url, path))

def checkout(repository_branch, path):
    """checkout in branch"""
    job = Popen(['git', 'checkout', repository_branch], stdout=PIPE, stderr=STDOUT, cwd=path)
    output, _ = job.communicate()
    if job.returncode != 0:
        raise BobError("Can not checkout into branch")

def get_repository_name(repository_url):
    r_name = urlparse(repository_url)
    r_name = r_name.path
    r_name = os.path.split(r_name)
    r_name = r_name[1]
    r_name = r_name.split('.')
    r_name = r_name[0]
    return r_name
