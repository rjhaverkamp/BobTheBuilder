# BSD 3-Clause License

# Copyright (c) 2016, Rob Haverkamp
# All rights reserved.

class BobError(Exception):
    """use for all errors related to BobTheBuilder"""
    pass