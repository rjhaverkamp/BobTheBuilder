# BobTheBuilder
BobTheBuilder is a simple continious integration project.

The idea is that you can build your own interface around it.
We try to keep it native python without dependencies or extra servers

The goal is that you can integrate it in every project or webframework
and can test any language on it.

# git

Needed dictionary:

    data = {
        'git': {
            'repository_url': 'https://github.com/rjrhaverkamp/test-bob.git',
            'repository_branch': 'master',
            'repository_path': '/tmp/test-bob/',
        },
        'clean_data': True
    }

# docker

a sample on how to get the code running is in `docker_example.py`  

Needed dictionary for code:

        data = {
            'docker': {
                'image': 'ubuntu',
                'pull': True,
                'command': "/bin/echo 1"
            }
        }

It is very simple, the dictionary translates to: `docker pull ubuntu` and `docker run -it --rm ubuntu /bin/echo 1`

        data = {
            'docker': {
                'image': 'python:3',
                'pull': True,
                'command': "python -c 'print(1)'"
            }
        }
will also work. There are some issues with quoting the commands so be careful!