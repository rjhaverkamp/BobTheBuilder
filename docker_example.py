from bob.build import buildbob
from bob.message import wendy
import pprint

data = {
    'docker': {
        'image': 'docker-test-build',
        'git_repository': 'https://github.com/teg/docker-test.git', # location to clone dockerfile from
        'build': True, # this will build, create and run a container from a dockerfile
        'pull': False,
        'command': '/bin/echo "hallo!"' # you can leave this out if building from dockerfile.
    }
}

wendy.init_wendy()
mqueue = wendy.mqueue
buildbob.init_bob(mqueue)
buildbob.queue.put(data)
pprint.pprint(mqueue.get())
mqueue.close()
buildbob.queue.close()