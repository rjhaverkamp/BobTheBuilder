import unittest
from os import path
import shutil

from bob.build import buildbob
from bob.message import wendy

class TestBuilds(unittest.TestCase):
    def test_simple_build(self):
        data = {
            'repository_url': 'https://github.com/rjrhaverkamp/test-bob.git',
            'repository_branch': 'master',
            'clean_data': False, # for testing purposes. Clean up after the test.
            'repository_path': '/tmp/test-bob/'
        }
        wendy.init_wendy()
        mqueue = wendy.mqueue
        buildbob.init_bob(mqueue)
        buildbob.queue.put(data)
        output = mqueue.get()
        self.assertEqual(output, {'status': 'succes', 'log': 'hoi\n'})
        mqueue.close()
        buildbob.queue.close()
        self.assertTrue(path.isdir('/tmp/test-bob'))
        shutil.rmtree(data['repository_path'])

if __name__ == '__main__':
    unittest.main()
